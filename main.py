from fastapi import FastAPI, Body, APIRouter, status, Header
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse

from typing import List
from datetime import datetime
import uvicorn

from database import database

app = FastAPI(title="REST API using FastAPI sqlite Async endpoits")


user_router = APIRouter()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)


@app.on_event('startup')
async def startup():
    await database.connect()

@app.on_event('shutdown')
async def shutdown():
    await database.disconnect()


app.include_router(user_router, prefix='/users')
if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8000)