from sqlalchemy import (
    create_engine,
)
from sqlalchemy.orm import sessionmaker
import databases

DATABASE = 'sqlite:///sqlite.db'


engine = create_engine(DATABASE, connect_args={"check_same_thread": False})
database = databases.Database(DATABASE)

Session = sessionmaker(bind = engine)
session = Session()